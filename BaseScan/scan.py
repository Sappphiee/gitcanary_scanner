import os
import shutil

from git import Repo, GitCommandError
from pydriller import RepositoryMining

from util import onerror, similar
from gitblamer import gitblame

def calculateChanges(mod):
    new = 0
    replaced = 0
    deleted = 0
    comment_new = 0
    comment_changed = 0
    comment_deleted = 0
    comment_start = ('/*', '*', '//')
    ignorelist = ["", '}']
    ignoreStart = ('package', 'import')
    added_lines = mod.diff_parsed['added']
    deleted_lines = mod.diff_parsed['deleted']
    for line in added_lines:
        if line[1].strip() not in ignorelist and not line[1].strip().startswith(ignoreStart):
            found = None
            for dline in deleted_lines:
                if similar(line[1], dline[1]) > 0.6:
                    found = dline
                    if line[1].strip().startswith(comment_start):
                        comment_changed += 1
                    else:
                        replaced += 1
            if not found:
                if line[1].strip().startswith(comment_start):
                    comment_new += 1
                else:
                    new += 1
            else:
                deleted_lines.remove(found)
    deleted += len([line for line in deleted_lines if
                    not line[1].strip().startswith(comment_start) and line[1].strip() not in ignorelist])
    comment_deleted += len([line for line in deleted_lines if line[1].strip().startswith(comment_start)])
    return {
        'code': {'new': new, 'changed': replaced, 'deleted': deleted},
        'comment': {'new': comment_new, 'changed': comment_changed, 'deleted': comment_deleted}
    }


def makeSerializable(commit, ignore):
    commitdata = {
        'hash': commit.hash,
        'author': commit.author.name,
        'date': commit.author_date.isoformat(),
        'dmm_unit_complexity':  0, #commit.dmm_unit_complexity,
        'dmm_unit_interfacing': 0, #commit.dmm_unit_interfacing,
        'dmm_unit_size': 0, #commit.dmm_unit_size,
        'merge': commit.merge,
        'message': commit.msg,
        'branches': list(commit.branches),
        'modifications': [],
        'noncode': 0
    }
    for mod in commit.modifications:
        if mod.new_path is not None:
            filename = mod.new_path
        elif mod.old_path is not None:
            filename = mod.old_path
        else:
            filename = mod.filename
        dirignore = False
        if 'directories' in ignore:
            dirignore = any(cfile in filename for cfile in ignore['directories'])
        if mod.language_supported and filename not in ignore['files'] and not dirignore and 'node_modules' not in filename and '__sapper__' not in filename:
            newmod = {
                'type': 'code',
                'filename': mod.filename,
                'raw_added': mod.added,
                'raw_removed': mod.removed,
                'calculated_changes': calculateChanges(mod),
                'nloc': mod.nloc,
                'complexity': mod.complexity,
                'methods': {},
                'methods_before': {}
            }
            for method in mod.changed_methods:
                newmod['methods'][method.long_name] = {
                    'nloc': method.nloc,
                    'complexity': method.complexity
                }
            for method in mod.methods_before:
                newmod['methods_before'][method.long_name] = {
                    'nloc': method.nloc,
                    'complexity': method.complexity
                }
            commitdata['modifications'].append(newmod)
        elif '.svelte' in mod.filename or '.dart' in mod.filename:
            newmod = {
                'type': 'code',
                'filename': mod.filename,
                'raw_added': mod.added,
                'raw_removed': mod.removed,
                'calculated_changes': calculateChanges(mod),
                'nloc': mod.nloc,
                'methods': {},
                'methods_before': {}
            }
            for method in mod.changed_methods:
                newmod['methods'][method.long_name] = {
                    'nloc': method.nloc,
                    'complexity': method.complexity
                }
            for method in mod.methods_before:
                newmod['methods_before'][method.long_name] = {
                    'nloc': method.nloc,
                    'complexity': method.complexity
                }
            commitdata['modifications'].append(newmod)
        elif '.md' in mod.filename:
            newmod = {
                'type': 'doc',
                'filename': mod.filename,
                'raw_added': mod.added,
                'raw_removed': mod.removed,
                'calculated_changes': calculateChanges(mod)['code']
            }
            commitdata['modifications'].append(newmod)
        else:
            commitdata['noncode'] += 1
    return commitdata

def scan(url, location, ignore):
    mastercommits = []
    commits = []
    repo = RepositoryMining(location)
    for commit in repo.traverse_commits():
        if commit.author.name not in ignore['authors'] and commit.hash not in ignore['commits']:
            #print(commit.hash)
            commits.append(makeSerializable(commit, ignore))
        mastercommits.append(commit.hash)

    pulled_repo = Repo(location)
    for b in pulled_repo.remote().fetch():
        try:
            #print(b.name)
            pulled_repo.git.checkout('-B', b.name.split('/')[1], b.name, force=True)
            for commit in repo.traverse_commits():
                if commit.hash not in mastercommits and commit.hash not in ignore['commits']:
                    #print(commit.hash)
                    commits.append(makeSerializable(commit, ignore))
                    mastercommits.append(commit.hash)
        except GitCommandError as e:
            print(e)
            print("Something went wrong with this commit: " + b.name)
    # shutil.rmtree(location, onerror=onerror)
    return {'commits': commits}

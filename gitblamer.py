import os
import subprocess
import re
from pydriller import GitRepository

def gitblame(location, ignore):
    working_dir = os.getcwd()
    committerlist = {}
    commentlist = {}
    mdlist = {}
    os.chdir(location)
    codefiles = ['.java', '.py', '.php', '.js', '.c', '.cpp', '.rb', '.ts', '.vue', '.svelte']
    comment_start = ('/*', '*', '//')
    ignorelist = ["", '}']
    ignoreStart = ('package', 'import')
    authors = {}
    for file in GitRepository(location).files():
        if any(cfile in file for cfile in codefiles):
            result = subprocess.Popen(['git','blame','--line-porcelain','-n',file], stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE)
            out, err = result.communicate()
            out = out.decode(encoding='utf-8', errors='replace')
            sha = [s.split()[0] for s in out.splitlines() if re.search(r'^[0-9a-f]{40} [0-9]+ [0-9]+ *[0-9]*$',s)]
            committers = [s for s in out.splitlines() if s.startswith("author ")]
            emails = [s for s in out.splitlines() if s.startswith("author-mail ")]
            lines = [s for s in out.splitlines() if "\t" in s]
            #assert len(lines) == len(sha)
            #assert len(lines) == len(emails)
            #assert len(lines) == len(committers)
            for num, committer in enumerate(emails, start=0):
                email = committer.replace('author-email ', '')
                if email != '<frank@paiq.nl>' and sha[num] not in ignore['commits']:
                    if email not in authors:
                        authors[email] = committers[num].replace('author ', '')
                    comi = authors[email]
                    if lines[num].strip().startswith(comment_start):
                        if comi not in commentlist.keys():
                            commentlist[comi] = 1
                        else:
                            commentlist[comi] += 1
                    else:
                        if lines[num].strip() not in ignorelist and not lines[num].strip().startswith(ignoreStart):
                            if comi not in committerlist.keys():
                                committerlist[comi] = 1
                            else:
                                committerlist[comi] += 1
        elif '.md' in file and not any (ffile in file for ffile in ['git-flow.md', 'library-documentation.md', 'technical-design.md']):
            result = subprocess.Popen(['git','blame','-n','--line-porcelain',file], stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE)
            out, err = result.communicate()
            out = out.decode(encoding='utf-8', errors='replace')
            sha = [s.split()[0] for s in out.splitlines() if re.search(r'^[0-9a-f]{40} [0-9]+ [0-9]+ *[0-9]*$',s)]
            committers = [s for s in out.splitlines() if s.startswith("author ")]
            emails = [s for s in out.splitlines() if s.startswith("author-mail ")]
            assert len(sha) == len(committers)
            assert len(sha) == len(emails)
            for num, committer in enumerate(emails, start=0):
                email = committer.replace('author-email ', '')
                if email != 'author-mail <frank@paiq.nl>' and sha[num] not in ignore['commits']:
                    if email not in authors:
                        authors[email] = committers[num].replace('author ', '')
                    comi = authors[email]
                    if comi not in mdlist.keys():
                        mdlist[comi] = 1
                    else:
                        mdlist[comi] += 1
    os.chdir(working_dir)
    return {'code': committerlist,'comments': commentlist, 'docs': mdlist}
